# Contribuer à Mixaconf

Merci de vouloir contribuer à Mixaconf :tada : Toutes les contributions à Mixaconf sont appréciées et encouragées.

## Table des matières

1. [Code de conduite](#code-de-conduite)
2. [Comment contribuer](#how-to-contribute)
    1. [Créer des issues](#creating-issues)
    2. [Ouvrir des pull requests](#opening-pull-requests)
    3. [Règles JS](#js-rules)
4. [Comment déployer en local](#how-to-run-locally)

## Code de conduite

Ce projet et tous ceux qui y participent sont régis par le [code de conduite](https://framagit.org/Miguel-WebDev/mixaconf/-/wikis/Code-De-Conduite).

En participant, vous êtes censé respecter ce code. Merci de signaler tout comportement inacceptable à [Recursiveness Open Projects](mailto:open@recursiveness.fr?subject=Comportement-COC).

## Comment contribuer

### Création de ticket (issue)

Avant de soumettre des issues, veuillez vérifier rapidement si elle n'existe pas déjà :
[Problèmes](https://framagit.org/Miguel-WebDev/mixaconf/-/issues).

Si vous ne trouvez pas de issue apparentée, veuillez en ouvrir une avec les étiquettes : `bug`, `documentation`, `feature-request` or `question`.

### Ouverture de pull requests

Les pull requests sont plus que bienvenues !

Assurez-vous simplement d'inclure une description du problème et de la manière dont vous essayez de le résoudre, ou bien suivez simplement le modèle de pull requests.

Nous exigeons également que les demandes de modification soient synchronisées avec la branche "master" via rebase (et non merge).

Donc, lorsque vous avez besoin de synchroniser votre branche avec master, utilisez : `git pull --rebase origin master`, ou si vous avez besoin de vous synchroniser avec une autre branche `git pull --rebase origin autre-branche-desirée`.

En faisant cela, vous supprimerez un commit de fusion supplémentaire dans l'historique git. Cela nécessitera également un push forcé vers la branche, par exemple `git push -u origin +une-branche`. *Le `+` dans la dernière commande indique que vous forcez les changements.*

De plus, nous demandons que les commits soient atomiques et écrasés si nécessaire. Cela permettra de garder l'historique git propre sur master. Pour écraser les commits, utilisez la commande `git rebase -i @~2` pour faire un rebase interactif.
 
Cela vous permettra de fusionner plusieurs commits en un seul. Pour en savoir plus sur ce sujet, veuillez visiter : [Git Tools Rewriting History](https://git-scm.com/book/en/v2/Git-Tools-Rewriting-History)

### Règles JS

Veuillez suivre la convention des bibliothèques et utiliser ces règles

```js
// En cours
```

### Comment exécuter localement

Pour l'instant, le projet est à l'étape *Non-fonctionnelle*.
Nous travaillons encore à la mise en place de pages interactives d'exemples.


### MIT Credits

* LeaderLine v1.0.5 : [anseki](https://anseki.github.io/leader-line/)
* [@philnash](https://twitter.com/philnash)