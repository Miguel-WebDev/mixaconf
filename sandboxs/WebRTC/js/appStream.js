// ###################################################################
// Développement : Luis M. M. (2021)
// ###################################################################



// Contraints for video/audio streaming
//  var constraints = { audio: true, video: { width: 1280, height: 720 } }; // Prefer camera resolution nearest to 1280x720.
let constraints = { audio: true, video: true };

// Connection and streaming button
document.querySelector('#start').addEventListener('click', function (e) {
    checkBrowser(); // Checking for old and bizar Web browsers
    startStream();
})


// ###################################################################
// FUNCTIONS
// ###################################################################
function checkBrowser() {
    // Older browsers might not implement mediaDevices at all, so we set an empty object first
    if (navigator.mediaDevices === undefined) {
        navigator.mediaDevices = {};
    }

    // Assignement of getUserMedia for some browsers.
    if (navigator.mediaDevices.getUserMedia === undefined) {
        navigator.mediaDevices.getUserMedia = function (constraints) {

            // First get ahold of the legacy getUserMedia, if present
            let getUserMedia = navigator.webkitGetUserMedia || navigator.mozGetUserMedia;

            // Some browsers just don't implement it - return a rejected promise with an error
            // to keep a consistent interface
            if (!getUserMedia) {
                return Promise.reject(new Error('getUserMedia is not implemented in this browser'));
            }

            // Otherwise, wrap the call to the old navigator.getUserMedia with a Promise
            return new Promise(function (resolve, reject) {
                getUserMedia.call(navigator, constraints, resolve, reject);
            });
        }
    }
};

// ************************************************
// Ecouter events sur flux
function bindEvents(c) { // connection
    // Envoie de l'offre pour ouvrir communication
    c.on('signal', function (data) {
        //debugger
    });

}

// ************************************************
// opening, closing, and broadcasting streams
function startStream() {
    navigator.mediaDevices.getUserMedia({ audio: true, video: true }).then(function (stream) {

    let sp = new SimplePeer({
        initiator: true,
        stream: stream
        //config : XXXXXX  // Server TURN & STUN
    })
    
    bindEvents(sp);
    
    
    let emitterVideo = document.querySelector('#emitter-video'); // ENVOIE
    emitterVideo.volume = 0;
    //debbuger
    
    // Older browsers may not have srcObject
    if ("srcObject" in emitterVideo) {
        emitterVideo.srcObject = stream;
    } else {
        // Avoid using this in new browsers, as it is going away.
        emitterVideo.src = window.URL.createObjectURL(stream);
    }
    emitterVideo.onloadedmetadata = function (e) { emitterVideo.play(); };
    })
    .catch(function (err) {
        console.log(err.name + ": " + err.message);
    });
};