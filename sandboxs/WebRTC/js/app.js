window.addEventListener('load', function () {
    'use strict';

// ======================================================================
// PARAMETRES
// ======================================================================
    const video = document.getElementById('video');
    const button = document.getElementById('button');

// ======================================================================

    if ('MediaStreamTrackProcessor' in window && 'MediaStreamTrackGenerator' in window) {
        console.log("MEDIA STREAM OK");
    }
    else console.log("MEDIA STREAM NOK");




    if (!navigator.mediaDevices || !navigator.mediaDevices.enumerateDevices) {
        console.log("enumerateDevices() not supported.");
        return;
    }

// ======================================================================


// ======================================================================

    // Fournis la liste des vid�o et audio connect�
    function getConnectedDevices(type, callback) {
        navigator.mediaDevices.enumerateDevices()
            .then(devices => {
                const filtered = devices.filter(device => device.kind === type);
                callback(filtered);
            });
    }



    getConnectedDevices('videoinput', cameras => console.log('Cameras found', cameras));
    getConnectedDevices('audioinput', audio => console.log('Audio found', audio.name));

    // Lance la VIDEO et AUDIO directe
    /*
        const openMediaDevices = async (constraints) => {
            return await navigator.mediaDevices.getUserMedia(constraints);
        }
    
        try {
            const stream = openMediaDevices({ 'video': true, 'audio': true });
            console.log('Got MediaStream:', stream);
        } catch (error) {
            console.error('Error accessing media devices.', error);
        }
    */

// ======================================================================


    // List cameras and microphones.
    navigator.mediaDevices.enumerateDevices()
        .then(function (devices) {
            devices.forEach(function (device) {
                console.log(device.kind + ": " + device.label +
                    " id = " + device.deviceId + " => name = " + device.name );
            });
        })
        .catch(function (err) {
            console.log(err.name + ": " + err.message);
        });
})