// author : @philnash : https://twitter.com/philnash
// Modifications : L.M.M.

const video = document.getElementById('video');

const buttonVid = document.getElementById('button-video');
const buttonAudio = document.getElementById('button-audio');
const selectVideo    = document.getElementById('select-video');
const selectAudio  = document.getElementById('select-audio');

const divVideo = document.getElementById('video-select');
const divAudio = document.getElementById('audio-select');

let currentStream;

function stopMediaTracks(stream) {
  stream.getTracks().forEach(track => {
    track.stop();
  });
}

// got Video
function gotVideoDevices(mediaDevices) {
  selectVideo.innerHTML = '';
  selectVideo.appendChild(document.createElement('option'));
  let count = 1;
    mediaDevices.forEach(mediaDevice => {
      if (mediaDevice.kind === 'videoinput') {
          const option = document.createElement('option');
          option.value = mediaDevice.deviceId;
          const label = mediaDevice.label || `Camera ${count++}`;
          const textNode = document.createTextNode(label);
          option.appendChild(textNode);
          selectVideo.appendChild(option);
      }
  });
}

// got Audio
function gotAudioDevices(mediaDevices) {
    selectAudio.innerHTML = '';
    selectAudio.appendChild(document.createElement('option'));
    let count = 1;
    mediaDevices.forEach(mediaDevice => {
        if (mediaDevice.kind === 'audioinput') {
            const option = document.createElement('option');
            option.value = mediaDevice.deviceId;
            const label = mediaDevice.label || `Audio ${count++}`;
            const textNode = document.createTextNode(label);
            option.appendChild(textNode);
            selectAudio.appendChild(option);
        }
    });
}

// ************* video
buttonVid.addEventListener('click', event => {
  if (typeof currentStream !== 'undefined') {
    stopMediaTracks(currentStream);
  }
  const videoConstraints = {};
    if (selectVideo.value === '') {
    videoConstraints.facingMode = 'environment';
  } else {
        videoConstraints.deviceId = { exact: selectVideo.value };
        console.log("NOM : " + { exact: selectVideo.value });
  }
  const constraints = {
    video: videoConstraints,
    audio: false
  };
  navigator.mediaDevices
    .getUserMedia(constraints)
    .then(stream => {
      currentStream = stream;
      video.srcObject = stream;
      return navigator.mediaDevices.enumerateDevices();
    })
      .then(gotVideoDevices)
    .catch(error => {
      console.error(error);
    });
});


// ************* audio
buttonAudio.addEventListener('click', event => {
    if (typeof currentStream !== 'undefined') {
        stopMediaTracks(currentStream);
    }
    const audioConstraints = {};
    if (selectAudio.value === '') {
        audioConstraints.facingMode = 'environment';
    } else {
        audioConstraints.deviceId = { exact: selectAudio.value };
    }
    const constraints = {
        video: false,
        audio: audioConstraints
    };
    navigator.mediaDevices
        .getUserMedia(constraints)
        .then(stream => {
            currentStream = stream;
            audio.srcObject = stream;
            return navigator.mediaDevices.enumerateDevices();
        })
        .then(gotAudioDevices)
        .catch(error => {
            console.error(error);
        });
});

navigator.mediaDevices.enumerateDevices().then(gotVideoDevices);
navigator.mediaDevices.enumerateDevices().then(gotAudioDevices);
