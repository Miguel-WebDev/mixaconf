if (document.addEventListener) {
    document.addEventListener("click", handleClick, false);
}
else if (document.attachEvent) {
    document.attachEvent("onclick", handleClick);
}

function handleClick(event) {
    event = event || window.event;
    event.target = event.target || event.srcElement;

    let element = event.target;

    //Climb up the document tree from the target of the event
    while (element) {
        if (element.nodeName === "BUTTON" && /connect/.test(element.className)) {
            //The user clicked on a <button> or clicked on an element inside a <button>
            //with a class name called "foo"
            doSomething(element);
        }

        element = element.parentNode;
    }
}

function doSomething(button) {
    console.log("Bouh le bouton !");
}


// =============================

btn.addEventListener('click', () => {
    console.log(btn.className);
    if (btn.className === "connect") {
        if (btn.value != 'Connecté') {
            btn.value = 'En cours';
            sleep(2000);
            txt.textContent = 'Connecté';
        } else {
            console.log("nouvelle source");
        }
    }
    else if (btn.className === "disconnect") {
        if (btn.value != 'Déconnecté') {
            btn.value = 'En cours';
            sleep(2000);
            txt.textContent = 'Déconnecté';
        } else {
            console.log("nouvelle source");
        }
    }
    else {}
}, null);
