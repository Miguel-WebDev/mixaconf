// Parameters
let streamIsOk = false;
let streams = [];
let cptStreams = 0;
let nbrConnections = 0;

window.addEventListener('load', function () {
    'use strict';


// =============================================================================

// gérer le clic sur les inputs et outputs
/*
window.addEventListener('load', function () {
    'use strict';





});



document.getElementsByClassName('source').addEventListener('click', event => {
    streams.push(["micro-HC251", "ZOOM"])
    button.innerHTML = `Nombre de clics : ${event.detail}`;
});
*/

// =============================================================================

// Fleches boites (x6) - ex-060
/*
window.addEventListener('load', function() {
    'use strict';

    var target1 = document.getElementById('ex-050-draggable'),
        target2 = document.getElementById('ex-050-draggable-in'),
        wallBBox = document.getElementById('sources').getBoundingClientRect(),
        line;

        wallBBox = {
            top: wallBBox.top + window.pageYOffset - target1.getBoundingClientRect().height,
            right: wallBBox.right + window.pageXOffset
        };

    new PlainDraggable(target1, {
        onDrag: function(moveTo) {
            var rect = this.rect;
            if (moveTo.left < wallBBox.right && moveTo.top > wallBBox.top) { // Inside
                if (rect.left >= wallBBox.right) {
                    moveTo.left = wallBBox.right;
                } else if (rect.top <= wallBBox.top) { // Avoid fixing both left and top.
                    moveTo.top = wallBBox.top;
                }
            }
        },
        onMove: function() { line.position(); },
        onMoveStart: function() { line.dash = {animation: true}; },
        onDragEnd: function() { line.dash = false; }
    });

    new PlainDraggable(target2, {
        onMove: function() { line.position(); },
        onMoveStart: function() { line.dash = {animation: true}; },
        onDragEnd: function() { line.dash = false; },
        zIndex: false
    });

    line = new LeaderLine(target2, target1,
    {startPlug: 'disc', endLabel: LeaderLine.pathLabel('ELEMENT', {fontFamily: 'Arial, sans-serif'})});
});
*/

// =============================================================================

// Module collant dans cellule - 30

var target = document.getElementById('e30-draggable'),
bBox = target.getBoundingClientRect(),
port = document.getElementById('e30-port');

port.style.width = bBox.width + 'px';
port.style.height = bBox.height + 'px';

new PlainDraggable(target, {
snap: [
  {x: 40, y: 64},
  {x: {end: 180}, y: 160},
  {x: {step: 40, start: 180}, y: {step: 40}}
]
});
// =============================================================================

// Fleches boites (x6) - 80
    
    let element1 = document.getElementById('80-1'),
        element2 = document.getElementById('80-2'),
        element3 = document.getElementById('80-3'),
        element4 = document.getElementById('80-4'),
        element5 = document.getElementById('80-5'),
        element6 = document.getElementById('80-6');

    // Dégradé couleur
    new LeaderLine(element1, element2, {
        startPlugColor  : '#1a6be0',
        endPlugColor    : '#1efdaa',
        gradient: true
    });

    // Une couleur (principale) ombrée
    new LeaderLine(element4, element5, {dropShadow: true});

    // pointillés fixes
    new LeaderLine(element5, element6, {dash: true});

    // pointillées animés
    let liner = new LeaderLine(element2, element3);
    inView('#line80').on('enter', function() { liner.dash = {animation: true}; })
        .on('exit', function () { liner.dash = false; });



// =============================================================================
    // Fleche animées transition 2 drag

    let target1 = document.getElementById('e50-draggable'),
        target2 = document.getElementById('e50-draggable-in'),
        wallBBox = document.getElementById('e50-stage-in').getBoundingClientRect(),
        line;

    wallBBox = {
        top: wallBBox.top + window.pageYOffset - target1.getBoundingClientRect().height,
        right: wallBBox.right + window.pageXOffset
    };

    new PlainDraggable(target1, {
        onDrag: function (moveTo) {
            let rect = this.rect;
            if (moveTo.left < wallBBox.right && moveTo.top > wallBBox.top) { // Inside
                if (rect.left >= wallBBox.right) {
                    moveTo.left = wallBBox.right;
                } else if (rect.top <= wallBBox.top) { // Avoid fixing both left and top.
                    moveTo.top = wallBBox.top;
                }
            }
        },
        onMove: function () { line.position(); },
        onMoveStart: function () { line.dash = { animation: true }; },
        onDragEnd: function () { line.dash = false; }
    });

    new PlainDraggable(target2, {
        onMove: function () { line.position(); },
        onMoveStart: function () { line.dash = { animation: true }; },
        onDragEnd: function () { line.dash = false; },
        zIndex: false
    });

    line = new LeaderLine(target2, target1,
        { startPlug: 'disc', endLabel: LeaderLine.pathLabel('ELEMENT', { fontFamily: 'Arial, sans-serif' }) });




// =============================================================================

    // drag&drop blocs
    // répéter chaque fois qu'on ajoute un input ou ouput...

    // *********************************************************
    let backColor = "#37506d";
    let backColorNew = "#ca4d4d";

    let inputState1 = document.getElementById('input-state-1');
    let inputState2 = document.getElementById('input-state-2');

    // input 1
    new PlainDraggable(inputState1, {
        onMoveStart: function () {
            //console.log("Je bouge : " + inputState1.className);
            inputState1.style.backgroundColor = backColorNew;
        },
        onDragEnd: function () {
            //console.log("Je bouge : " + inputState1.className);
            inputState1.style.backgroundColor = backColor;
        },
        handle: document.querySelector('#input-state-1 .draggable')
    });

    // input 2
    new PlainDraggable(inputState2, {
        onMoveStart: function () {
            //console.log("Je bouge : " + inputState2.className);
            inputState2.style.backgroundColor = backColorNew;
        },
        onDragEnd: function () {
            //console.log("Je bouge : " + inputState2.className);
            inputState2.style.backgroundColor = backColor;
        },
        handle: document.querySelector('#input-state-2 .draggable') 
    });

    // *********************************************************
    let outputState1 = document.getElementById('output-state-1');
    let outputState2 = document.getElementById('output-state-2');

    // input 1
    new PlainDraggable(outputState1, {
        onMoveStart: function () {
            //console.log("Je bouge : " + outputState1.className);
            outputState1.style.backgroundColor = backColorNew;
        },
        onDragEnd: function () {
            //console.log("Je bouge : " + outputState1.className);
            outputState1.style.backgroundColor = backColor;
        },
        handle: document.querySelector('#output-state-1 .draggable')
    });

    // input 2
    new PlainDraggable(outputState2, {
        onMoveStart: function () {
            //console.log("Je bouge : " + outputState2.className);
            outputState2.style.backgroundColor = backColorNew;
        },
        onDragEnd: function () {
            //console.log("Je bouge : " + outputState2.className);
            outputState2.style.backgroundColor = backColor;
        },
        handle: document.querySelector('#output-state-2 .draggable')
    });

// =============================================================================


    //let btnConnect      = document.querySelector('button').getElementsByClassName('connect');
    //let btnDisconnect   = document.querySelector('button').getElementsByClassName('disconnect');
    let btn = document.querySelector('button');
    let txt = document.querySelector('p');

    //btnConnect.addEventListener('click', updateBtnConnect(btnConnect, txt));

    //btnDisconnect.addEventListener('click', updateBtnDisconnect(btnDisconnect, txt));
    
    
    // addEventListener("click", function(){modifyText("quatre")}, false);



});

// Quand Click sur Bouton "Connection" des INPUTS/OUTPUTS
function coBtn () {
    let txt = document.querySelector('p');
    if (txt.value != 'Connecté') {

        // PARTIE DEDIE : Connecter input et output
        // SOLUTION UNE
        // NON FONCTIONNELLE
        console.log("OK : Lance connection");
        let outputs = document.getElementsByClassName("outputs");

        document.getElementsByClassName("outputs").onclick = function() {
            var elt = this;
            console.log("outputs : " + this);
            console.log("click : " + this.getAttribute('id'));
        };
        // FIN SOLUTION

        txt.value = 'En cours';
        sleep(2000);
        txt.textContent = 'Connecté';
    } else {
        console.log("nouvelle source");
    }
}

// Quand Click sur Bouton "Deconnection" des INPUTS/OUTPUTS
function decoBtn () {
    let txt = document.querySelector('p');
    if (txt.value != 'Déconnecté') {
        txt.value = 'En cours';
        sleep(2000);
        txt.textContent = 'Déconnecté';
    } else {
        console.log("nouvelle source");
    }
}







function sleep(value) {
    setTimeout(function(){
    }, value);
}

