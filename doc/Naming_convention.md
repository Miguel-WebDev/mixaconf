# Les Commit

Les commit doivent suivre une nomenclature propre au projet pour plus de careté pour tous. Merci par avance pour leur application.

**Un commit doit obligatoirement être accompagné d'un message.**

Chaque message doit utiliser des mots clés de mofication selon sa nature :
**Pour les fichiers en général** : ADD, UPDATE, REMOVE,
**Pour les répertoires / fonctionnalités** : CREATE, UPDATE, SUPPR

* Vous pouvez également indiquer quel fichier a été ajoutée, supprimée, modifiée après **":"** :
*Ex.* : "REMOVE **:** readme.md"

* **Vous devez ajouter** une courte description des actions réalisées **":"** :
*Ex.* : "ADD **:** readme.md for project présentation"

* Vous pouvez indiquer un lien tacite entre des fichiers ET/OU action avec **">"** :
*Ex. après un "add src/test1* : UPDATE : src/test1 **>** test2 = feature update "

* Vous pouvez inclure des sous "actions réalisée" avec **'action'** :
*Ex. "UPDATE : readme = **'add'** english version"

**Vous pouvez utiliser votre bon sens et votre goût personnel** mais il est **important** de toujours garder à l'esprit que nous devons tous utiliser une approche claire, cohérente avec le propos et semblable.

