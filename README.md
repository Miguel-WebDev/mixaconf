# English

# Open Source Software Development (OSSD) - M2 MIAGE 2021-2022 - Paris-Saclay

## What?
**Open Source Software Development: Software Audio/Video Multiplexing and Mixing

This is a MIAGE student project launched in 2021, of a **software solution for software audio/video mixing and multiplexing on web technologies**, which is meant to be accessible and free (CC BY-NC-SA).

**This conductor is intended to provide a dynamic and easy-to-use tool** to *associations* that have a need for *conference management and training broadcasting*.

The broadcasting remains free of any tool but this software will allow to acquire the streams and to configure them upstream or live in a **visual** way.

The ability to save the configuration in XML, to import and export it will allow this solution to be interfaced with other software and hardware, in a simple way.

## How to use it ?

At this stage of development the product is not functional in its entirety. There are ways to test and experiment.

**Here are the pages allowing to test different functionalities:**

* To test the connection display of clients and managers: mixaconf/sources/index.html
* To test the display of the visual programming of flows: mixaconf/sandboxs/visual-config/index.html
* To test the retrieval of available devices and display/listen to them: mixaconf/sandboxs/WebRTC/index.html

## For whom?
Any association, charity, volunteer or school structure with a specific need for the management and configuration of audio and video broadcasting as part of the sharing of knowledge and ideas.


## By whom?
Project idea and need submitted by the **Université du Temps Libre (UTL)** [UTL Essonne](https://www.utl-essonne.org/ "Université du Temps Libre")
Maintainer of the project with initial upload and implementation:  **Luis M. MARTINS** (MIAGE student 2020-2022)

**Strategic leaders of the project
- Mr L.M.M.
- Mr D. C.
- Mr J.R.

## Guide and tutorials
User support is provided in documentation [TO BE DEVELOPED] and video [TO BE DEVELOPED].

## How to contribute?

Find our contribution information in this section : [Contributing](https://framagit.org/Miguel-WebDev/mixaconf/-/wikis/Contributing-FR)

**Thank You !

# French

# Développement de Logiciel Libre (DLL) - M2 MIAGE 2021-2022 - Paris-Saclay

## Quoi ?
**Développement de Logiciel Libre : Multiplexage et mixage audio/vidéo logiciel**

Ceci est un projet étudiant MIAGE lancé en 2021, d'une **solution logiciel de mixage et multiplexage audio / vidéo logiciel sur technologies Web**, qui se veut accessible et libre (CC BY-NC-SA).

**Ce chef d'orchestre à pour vocation à fournir un outil dynamique et simple d'utilisation** aux *associations* qui ont un besoin en *gestion de conférence et formation*.

La diffusion reste libre de tout outil mais ce logiciel permettra d'acquérir les flux et de les configurer en amont ou en live de façon **visuel**.

La possibilité de sauvegarder la configuration en XML, de l'importer et de l'exporter permettra d'interfacer cette solution avec d'autre logiciels et matériel, de façon simple.

## Pour qui ?
Toute structure associative, caritative, de bénévolat ou scolaire ayant un besoin propre à la gestion et la configuration de diffusion audio et vidéo dans le cadre du partage des connaissances et des idées.

## Comment l'utiliser ?

A ce stade de développement le produit n'est pas fonctionnel dans son ensemble, des moyens de tests et d'expérimentation existent.

**Des pages existent pour tester différentes fonctionnalités :**

* Pour tester l'affichage de connection des clients et gestionnaires : mixaconf/sources/index.html
* Pour tester l'affichage de la programmation visuel des flux : mixaconf/sandboxs/visual-config/index.html
* Pour tester la récupération des périphériques disponibles et les afficher/écouter : mixaconf/sandboxs/WebRTC/index.html


## Par qui ?
Idée et besoin de projet soumit par L'**Université du Temps Libre (UTL)** [UTL Essonne](https://www.utl-essonne.org/ "Université du Temps Libre")

Mise en ligne et réalisation initiale : **Luis M. MARTINS** (Etudiant MIAGE 2020-2022)

**Responsables stratégique du projet**
- Monsieur L.M.M.
- Monsieur D. C.
- Monsieur J.R.

## Guide et tutoriels
Un support à l'utilisation est fourni, en documentation [A VENIR] et vidéo [A VENIR]

## Comment contribuer ?

Retrouver nos information de contribution dans la section [Contributing](https://framagit.org/Miguel-WebDev/mixaconf/-/wikis/Contributing-FR)

**Merci à vous !**
