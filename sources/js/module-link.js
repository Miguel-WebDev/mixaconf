/*  ---------------------------------------------
     Document initialisation
    --------------------------------------------- */
$(document).ready(function () {
    hideAlert();

    sessionStorage.setItem("nbrClients", 0);
    sessionStorage.setItem("ifAdminConnected", false);
    sessionStorage.setItem("admin", false);

    //getDevices();
});


/*  ---------------------------------------------
     Connection msg management for clients & admin
    --------------------------------------------- */
// Client connection test
$("#clientConnect").click(function () {
    let nbrClients = sessionStorage.getItem("nbrClients");
    if (nbrClients == 0) {
        sessionStorage.nbrClients++;
        $("#clientConnect").html("Connecter Client [1]");
        alertButton("alert-success");
    } else if (nbrClients > 0) {
        sessionStorage.nbrClients++;
        $("#clientConnect").html("Connecter Client [" + sessionStorage.getItem('nbrClients') + "]");
        alertButton("alert-secondary");
    } else alertButton("alert-danger"); // Error

    if (sessionStorage.ifAdminConnected == true) {
        alertButton("alert-dark");
    }
});


// Admin connection test
$("#adminConnect").click(function () {
    let adminStatus = sessionStorage.getItem("ifAdminConnected")

    if (adminStatus === 'false') {
        alertButton("alert-primary");
        sessionStorage.ifAdminConnected = true;
        $("#adminConnect").html("Connecter Gestionnaire [V]");
    } else if (adminStatus === 'true') {
        alertButton("alert-warning");
    } else alertButton("alert-danger"); // Error

    if (sessionStorage.ifAdminConnected == true) {
        alertButton("alert-dark");
    }
});

// Client DEconnection test
$("#clientDisconnect").click(function () {
    let nbrClients = sessionStorage.getItem("nbrClients");
    if (nbrClients > 0) {
        sessionStorage.nbrClients--;
        $("#clientConnect").html("Connecter Client [" + sessionStorage.getItem('nbrClients') + "]");
        alertButton("alert-infoC");
    }

});

// Admin DEconnection test
$("#adminDisconnect").click(function () {
    if (sessionStorage.getItem("ifAdminConnected") === 'true') {
        sessionStorage.ifAdminConnected = false;
        $("#adminConnect").html("Connecter Gestionnaire [X]");
        alertButton("alert-infoA");
    }

});

/*  ---------------------------------------------
      Alert msgs management function
    --------------------------------------------- */

 // Hide alert messages
function alertButton(alertMsg) {
    hideAlert();
    if ($("#"+alertMsg+"").is(":hidden")) {
        $("#"+alertMsg+"").show();
    }
}

// Hide manually alert messages
function CloseAlert(alertMsg) {
    $("#alert-"+alertMsg+"").hide();
}


function hideAlert()
{ $(".alert").hide(); }