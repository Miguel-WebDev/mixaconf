# Creative Commons BY-NC-SA

## English

**Attribution-NonCommercial-ShareAlike 4.0 International (CC BY-NC-SA 4.0)**

* Share — copy and redistribute the material in any medium or format
* Adapt — remix, transform, and build upon the material

*The licensor cannot revoke these freedoms as long as you follow the license terms.*

**Under the following terms**

**Attribution** — You must give appropriate credit, provide a link to the license, and indicate if changes were made. You may do so in any reasonable manner, but not in any way that suggests the licensor endorses you or your use.

**NonCommercial** — You may not use the material for commercial purposes.

**ShareAlike** — If you remix, transform, or build upon the material, you must distribute your contributions under the same license as the original.

**No additional restrictions** — You may not apply legal terms or technological measures that legally restrict others from doing anything the license permits.

## French

**Attribution - Pas d’Utilisation Commerciale - Partage dans les Mêmes Conditions 2.0 France (CC BY-NC-SA 2.0 FR)**

Vous êtes autorisé à :
* Partager — copier, distribuer et communiquer le matériel par tous moyens et sous tous formatsAdapter
* Remixer, transformer et créer à partir du matériel

*L'Offrant ne peut retirer les autorisations concédées par la licence tant que vous appliquez les termes de cette licence.*

**Selon les conditions suivantes**

**Attribution** — Vous devez créditer l'Œuvre, intégrer un lien vers la licence et indiquer si des modifications ont été effectuées à l'Oeuvre. Vous devez indiquer ces informations par tous les moyens raisonnables, sans toutefois suggérer que l'Offrant vous soutient ou soutient la façon dont vous avez utilisé son Oeuvre.

**Pas d’Utilisation Commerciale** — Vous n'êtes pas autorisé à faire un usage commercial de cette Oeuvre, tout ou partie du matériel la composant.

**Partage dans les Mêmes Conditions** — Dans le cas où vous effectuez un remix, que vous transformez, ou créez à partir du matériel composant l'Oeuvre originale, vous devez diffuser l'Oeuvre modifiée dans les même conditions, c'est à dire avec la même licence avec laquelle l'Oeuvre originale a été diffusée.

**Pas de restrictions complémentaires** — Vous n'êtes pas autorisé à appliquer des conditions légales ou des mesures techniques qui restreindraient légalement autrui à utiliser l'Oeuvre dans les conditions décrites par la licence.
